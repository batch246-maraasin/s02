year = int(input("Please input a year: \n"))

if year % 4 == 0:
	print(f"{year} is a leap year.")
else:
	print(f"{year} is not a leap year.")

print("--------------")

row = int(input("Enter the number of rows: "))
col = int(input("Enter the number of columns: "))

grid = ""
for i in range(row):
	for j in range(col):
		grid += "*"
	grid += "\n"

print(grid)

